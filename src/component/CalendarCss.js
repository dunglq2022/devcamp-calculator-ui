import { styled } from "styled-components";


export const Calendar = styled.div`
    background: linear-gradient(#fff, gray);
    height: 410px;
    width: 700px;
`

export const WrapperCalendar = styled.div`
    background: white;
    height:400px ;
    margin-left: 10px;
    margin-right: 10px;
    border-radius: 5px;
`

export const WrapperDay = styled.div`
    display: grid;
    grid-template-columns: repeat(7, 1fr);
    height: 100%;
    grid-gap: 20px
`

export const ButtonDay = styled.div`
    ${props => props.currentDay ? `
        background-color: #006edd;
        color: white;
    `: null}
    font-size: 22px;
    font-weight: ${props => props.fontWeight};
    color: ${props => props.fontColor};

`