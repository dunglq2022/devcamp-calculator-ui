import { WrapperCalendar, Calendar, WrapperDay, ButtonDay } from "./CalendarCss";

export function CalendarUI () {
    return(
        <Calendar>
            <WrapperCalendar>
                <div className="row">
                    <div className="col">
                        <div className="row" style={{fontFamily: 'Titan One'}}>
                            <div className="col text-center">
                            &lt;&lt;
                            </div>
                            <div className="col text-center">
                            &lt;
                            </div>
                            <div className="col-6 text-center">
                                <h5>Februay 2018</h5>
                            </div>
                            <div className="col text-center">
                            &gt;
                            </div>
                            <div className="col text-center">
                            &gt;&gt;
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col">
                        <WrapperDay className="text-center">
                            <ButtonDay fontWeight='bold'>SUN</ButtonDay>
                            <ButtonDay fontWeight='bold'>MON</ButtonDay>
                            <ButtonDay fontWeight='bold'>TUE</ButtonDay>
                            <ButtonDay fontWeight='bold'>WEN</ButtonDay>
                            <ButtonDay fontWeight='bold'>THU</ButtonDay>
                            <ButtonDay fontWeight='bold'>FRI</ButtonDay>
                            <ButtonDay fontWeight='bold'>SAT</ButtonDay>
                            <ButtonDay fontColor='gray'>28</ButtonDay>
                            <ButtonDay fontColor='gray'>29</ButtonDay>
                            <ButtonDay fontColor='gray'>30</ButtonDay>
                            <ButtonDay fontColor='gray'>31</ButtonDay>
                            <ButtonDay>1</ButtonDay>
                            <ButtonDay>2</ButtonDay>
                            <ButtonDay currentDay>3</ButtonDay>
                            <ButtonDay fontColor='red'>4</ButtonDay>
                            <ButtonDay>5</ButtonDay>
                            <ButtonDay>6</ButtonDay>
                            <ButtonDay>7</ButtonDay>
                            <ButtonDay>8</ButtonDay>
                            <ButtonDay>9</ButtonDay>
                            <ButtonDay fontColor='red'>10</ButtonDay>
                            <ButtonDay fontColor='red'>11</ButtonDay>
                            <ButtonDay>12</ButtonDay>
                            <ButtonDay>13</ButtonDay>
                            <ButtonDay>14</ButtonDay>
                            <ButtonDay>15</ButtonDay>
                            <ButtonDay>16</ButtonDay>
                            <ButtonDay fontColor='red'>17</ButtonDay>
                            <ButtonDay fontColor='red'>18</ButtonDay>
                            <ButtonDay>19</ButtonDay>
                            <ButtonDay>20</ButtonDay>
                            <ButtonDay>21</ButtonDay>
                            <ButtonDay>22</ButtonDay>
                            <ButtonDay>23</ButtonDay>
                            <ButtonDay fontColor='red'>24</ButtonDay>
                            <ButtonDay fontColor='red'>25</ButtonDay>
                            <ButtonDay>26</ButtonDay>
                            <ButtonDay>27</ButtonDay>
                            <ButtonDay>28</ButtonDay>
                            <ButtonDay fontColor='gray'>1</ButtonDay>
                            <ButtonDay fontColor='gray'>2</ButtonDay>
                            <ButtonDay fontColor='gray'>3</ButtonDay>
                        </WrapperDay>
                    </div>
                </div>
            </WrapperCalendar>
        </Calendar>
    )
}