import style from './Calculator.module.css'
import { RetageButton, SquareButton } from '../component/CalculatorCssV2'
export const CalculatorV2 = () => {
    return(
        <>
            <div className="container mt-4">
                <div className={style.Calculator}>
                    <div className={style.Wrapper}>
                        <div className={style.Display}></div>
                        <SquareButton>
                            <RetageButton radiusLeftTop fontColor='#818284' bgColor='#ced2d8' gridColumns>DEL</RetageButton>
                            <RetageButton fontColor='#818284' bgColor='#ced2d8'>{'<-'}</RetageButton>
                            <RetageButton radiusRightTop fontColor='#818284' bgColor='#b9d7d5'>/</RetageButton> 
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea'>7</RetageButton>
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea'>8</RetageButton>
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea'>9</RetageButton>
                            <RetageButton fontColor='#818284' bgColor='#b9d7d5'>*</RetageButton>    
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea'>4</RetageButton>
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea'>5</RetageButton>
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea'>6</RetageButton>
                            <RetageButton fontColor='#818284' bgColor='#b9d7d5'>-</RetageButton> 
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea'>1</RetageButton>
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea'>2</RetageButton>
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea'>3</RetageButton>
                            <RetageButton fontColor='#818284' bgColor='#b9d7d5'>+</RetageButton> 
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea' radiusLeftBottom>0</RetageButton>
                            <RetageButton fontColor='#569d97' bgColor='#e5e4ea'>,</RetageButton>
                            <RetageButton hoverColorSpecial='#b9d7d5' gadient gridColumns radiusRightBottom>=</RetageButton>                 
                        </SquareButton>
                    </div>
                </div>
            </div>
        </>
            
    )
}