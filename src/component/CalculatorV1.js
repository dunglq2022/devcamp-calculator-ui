import { ButtonChild, ButtonWrapper, Display, DisplayChild, Wrapper } from '../component/CalculatorCss';

export function CalculatorV1 () {
    return(
        <div>
        <Wrapper>
          <Display>
          2356 + 419 +
          </Display>
          <hr style={{color: 'gray', width: 330, marginLeft: 10, margin: 0}}/>
          <Display>
            <DisplayChild>2955</DisplayChild>
          </Display>
          <ButtonWrapper>
            <ButtonChild bgColor='#404d5e' fontColor='#d65956'>C</ButtonChild>
            <ButtonChild bgColor='#404d5e'>#</ButtonChild>
            <ButtonChild bgColor='#404d5e'>%</ButtonChild>
            <ButtonChild bgColor='#404d5e'>/</ButtonChild>
          </ButtonWrapper>
          <ButtonWrapper>
            <ButtonChild>7</ButtonChild>
            <ButtonChild>8</ButtonChild>
            <ButtonChild>9</ButtonChild>
            <ButtonChild bgColor='#404d5e'>X</ButtonChild>
          </ButtonWrapper>
          <ButtonWrapper>
            <ButtonChild>4</ButtonChild>
            <ButtonChild>5</ButtonChild>
            <ButtonChild>6</ButtonChild>
            <ButtonChild bgColor='#404d5e'>-</ButtonChild>
          </ButtonWrapper>
          <ButtonWrapper>
            <ButtonChild>1</ButtonChild>
            <ButtonChild>2</ButtonChild>
            <ButtonChild>3</ButtonChild>
            <ButtonChild bgColor='#404d5e'>+</ButtonChild>
          </ButtonWrapper>
          <ButtonWrapper>
            <ButtonChild>.</ButtonChild>
            <ButtonChild>0</ButtonChild>
            <ButtonChild>&lt;</ButtonChild>
            <ButtonChild bgColor='#404d5e'>=</ButtonChild>
          </ButtonWrapper>
        </Wrapper>
      </div>
    )
}