import { styled } from "styled-components";

export const Wrapper = styled.div`
    border: 1px solid 3a4655;
    background-color: #3a4655;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    width: 350px;
    height: 500px%;
`
export const Display = styled.div`
    padding: 10px 20px;
    font-size: 24px;
    text-align: right;
    color: gray;
`

export const DisplayChild = styled(Display)`
    font-size: 45px;
    color: white;
    padding-top: 0px;
`

export const ButtonWrapper = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 1px;
    border: 0px solid #404d5e;
    background-color: #3a4655;
    width: 350px;
    heght: 100%;
`

export const ButtonChild = styled.button`
    width: 100%;
    height: 70px;
    border: 0.1rem solid #3a4655;
    border-radius: 1px;
    font-size: 20px;
    font-weight: bold;
    background-color: ${props => props.bgColor || '#4f6074'};
    color: ${props => props.fontColor || 'white'};
    cursor: pointer;
    transition: all 0.3 ease;

    &:hover {
        background-color: #3a4655;
        color: #fff;
    }
`;