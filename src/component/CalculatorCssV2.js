import styled from "styled-components";
export const SquareButton = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 3px;
    height: 300px;
    width: 360px;
    background-color: #fff;
    position: absolute;
    top:170px;
    left: 10px;
    border-radius: 5px;
`
export const RetageButton = styled.button`
    border-radius: 0 1px 1px 0;
    border:none;
    width: 100%;
    height: 90px;
    font-size: 20px;
    background-color: ${props => props.bgColor || '#425062'};
    color: ${props => props.fontColor || 'white'};
    cursor: pointer;
    transition: all 0.3s ease;

    ${props =>
        props.gadient &&
        `
            background-image: linear-gradient(to right, #549b96, #6bb7aa);
        `}

    ${props =>
        props.radiusLeftTop && 
        `
            border-radius: 5px 0 0 0;
        `}

    ${props =>
        props.radiusRightTop && 
        `
            border-radius: 0 5px 0 0;
        `}
        
    ${props =>
        props.radiusLeftBottom && 
        `
            border-radius: 0 0 0 5px;
        `}

    ${props =>
        props.radiusRightBottom && 
        `
            border-radius: 0 0 5px 0;
        `}

    ${props =>
        props.gridColumns &&
        `
            grid-column: span 2;
        `}

    &:hover {
        background-color: ${props =>props.hoverColorSpecial || '#539895'} ;
        color: #fff;
    }
`;