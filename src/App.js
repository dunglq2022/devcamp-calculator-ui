import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { CalendarUI } from './component/CalendarUI';

function App() {
  return (
    <div style={{display: 'flex'}}>
      <CalendarUI></CalendarUI>
    </div>
  );
}

export default App;
